#include "wx/wx.h"
#include <wx/dcbuffer.h>
#include "ray.h"
#include "vec3.h"

//定义主类：继承wxApp
class MyApp : public wxApp
{
public:
	//覆盖OnInit函数
	virtual bool OnInit();
};

//定义Frame类：继承wxFrame
class MyFrame : public wxFrame
{
public:

	MyFrame(const wxString& title);//构造函数
	void OnQuit(wxCommandEvent& event);//退出时触发
	void OnMotion(wxMouseEvent& event);//鼠标拖拽触发
	void DrawRGB(wxPaintEvent& event);//重新绘画时触发
	void OnPaint(wxPaintEvent& event);//重新绘画时触发

private:
	DECLARE_EVENT_TABLE()
};

DECLARE_APP(MyApp)
IMPLEMENT_APP(MyApp)
/**
碰撞函数,接受光线返回颜色
*/
vec3 genColor(const ray& r){
    //unit是屏幕上的点的方向为单位向量
    vec3 unit_direction = unit_vector(r.direction());
    return vec3(1.75f - 0.25f * unit_direction.y(),1.85f - 0.15f * unit_direction.y(),2.0f);
    float t = 0.5f * (unit_direction.y() + 1.0f);
    return (1.0f - t)*vec3(1.0f, 1.0f, 1.0f) + t*vec3(0.5f, 0.7f, 1.0f);
    //上面的操作等效于return vec3(1.75f - 0.25f * unit_direction.y(),1.85f - 0.15f * unit_direction.y(),2.0f);
}
bool MyApp::OnInit()
{
	//关联Frame，初始化
	MyFrame* frame = new MyFrame(wxT("Minimal wxWidgets App"));
	frame->Show(true);
	frame->SetSize(600, 400);
	return true;
}


BEGIN_EVENT_TABLE(MyFrame, wxFrame)
EVT_MENU(wxID_EXIT, MyFrame::OnQuit)
EVT_MOTION(MyFrame::OnMotion)
EVT_PAINT(MyFrame::OnPaint)
END_EVENT_TABLE()

MyFrame::MyFrame(const wxString& title) : wxFrame(NULL, wxID_ANY, title) {}

void MyFrame::OnQuit(wxCommandEvent& event) {
	Close();
}

void MyFrame::OnMotion(wxMouseEvent& event)
{
	if (event.Dragging())
	{
		wxClientDC dc(this);
		wxPen pen(*wxGREEN, 2);
		dc.SetPen(pen);
		dc.DrawPoint(event.GetPosition());
		dc.SetPen(wxNullPen);

	}
}
//第2章向量类测试
void MyFrame::DrawRGB(wxPaintEvent& event) {
//需要引入 <wx/dcbuffer.h>
    //wxClientDC dc(this);
	wxBufferedPaintDC dc(this);//用这个会让背景变黑
    //窗口像素坐标长和宽
	wxCoord w = GetClientSize().x, h = GetClientSize().y;
	//左下角
    vec3 lower_left_corner(-2.0, 1.0, -1.0);
    //
    vec3 horizontal(4.0, 0.0, 0.0);
    vec3 vertical(0.0, 2.0, 0.0);
    //原点(相机点)
    vec3 origin(0.0, 0.0, 0.0);
	wxColour color(0, 0, 0, 255);
	wxPen pen(color);
	for (int x = 0; x < w; x++)
	{
		for (int y = 0; y < h; y++)
		{
            //在世界坐标中uv代表屏幕上的点
	        float u = float(x) / float(w);
            float v = float(y) / float(h);
            //生成光线(左下角点，遍历点)
            ray r(origin, lower_left_corner + u*horizontal - v*vertical);
            //生成光线的颜色
            vec3 col = genColor(r);

            int ir = int(255.99 * col[0]);
            int ig = int(255.99 * col[1]);
            int ib = int(255.99 * col[2]);

			color.Set(ir, ig, ib, 255);
			pen.SetColour(color);
			dc.SetPen(pen);
			dc.DrawPoint(x, y);
		}
	}

}
void MyFrame::OnPaint(wxPaintEvent& event){
	DrawRGB(event);
}


