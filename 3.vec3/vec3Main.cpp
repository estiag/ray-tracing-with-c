#include "wx/wx.h"
#include <wx/dcbuffer.h>

#include "vec3.h"

//定义主类：继承wxApp
class MyApp : public wxApp
{
public:
	//覆盖OnInit函数
	virtual bool OnInit();
};

//定义Frame类：继承wxFrame
class MyFrame : public wxFrame
{
public:

	MyFrame(const wxString& title);//构造函数
	void OnQuit(wxCommandEvent& event);//退出时触发
	void OnMotion(wxMouseEvent& event);//鼠标拖拽触发
	void DrawRGB(wxPaintEvent& event);//重新绘画时触发
	void OnPaint(wxPaintEvent& event);//重新绘画时触发
private:
	DECLARE_EVENT_TABLE()
};

DECLARE_APP(MyApp)
IMPLEMENT_APP(MyApp)

bool MyApp::OnInit()
{
	//关联Frame，初始化
	MyFrame* frame = new MyFrame(wxT("Minimal wxWidgets App"));
	frame->Show(true);
	frame->SetSize(600, 400);
	return true;
}


BEGIN_EVENT_TABLE(MyFrame, wxFrame)
EVT_MENU(wxID_EXIT, MyFrame::OnQuit)
EVT_MOTION(MyFrame::OnMotion)
EVT_PAINT(MyFrame::OnPaint)
END_EVENT_TABLE()

MyFrame::MyFrame(const wxString& title) : wxFrame(NULL, wxID_ANY, title) {}

void MyFrame::OnQuit(wxCommandEvent& event) {
	Close();
}

void MyFrame::OnMotion(wxMouseEvent& event)
{
	if (event.Dragging())
	{
		wxClientDC dc(this);
		wxPen pen(*wxGREEN, 2);
		dc.SetPen(pen);
		dc.DrawPoint(event.GetPosition());
		dc.SetPen(wxNullPen);

	}
}
//第2章向量类测试
void MyFrame::DrawRGB(wxPaintEvent& event) {
//需要引入 <wx/dcbuffer.h>
	wxBufferedPaintDC dc(this);//用这个会让背景变黑
	wxSize sz = GetClientSize();

	wxCoord w = sz.x, h = sz.y;
	for (int x = 0; x < w; x++)
	{
		for (int y = 0; y < h; y++)
		{

            vec3 col(float(x) / float(w), float(y) / float(h), 0.2f);
			int ir = int(255.99 * col[0]);
			int ig = int(255.99 * col[1]);
			int ib = int(255.99 * col[2]);
			dc.SetPen(wxPen(wxColour(ir, ig, ib, 255)));
			dc.DrawPoint(x, y);
		}
	}

}
void MyFrame::OnPaint(wxPaintEvent& event){
	DrawRGB(event);
}


