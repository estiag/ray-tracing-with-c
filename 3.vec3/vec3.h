#ifndef VEC3_H_INCLUDED
#define VEC3_H_INCLUDED

#include <math.h>
#include <stdlib.h>
#include <iostream>


#endif // VEC3_H_INCLUDED
class vec3
{
public:
    float e[3];
    vec3() {}
    vec3(float e0,float e1,float e2)
    {
        e[0] = e0;
        e[1] = e1;
        e[2] = e2;
    }
    inline float x() const
    {
        return e[0];
    }
    inline float y() const
    {
        return e[1];
    }
    inline float z() const
    {
        return e[2];
    }

    inline float r() const
    {
        return e[0];
    }
    inline float g() const
    {
        return e[1];
    }
    inline float b() const
    {
        return e[2];
    }
    //vec3&表示vec3类型的引用
    //operate操作符() 表示重载这个操作符,用函数赋予操作符新含义
    //函数括号后的const表示参数是引用传递
    //const 修饰类成员函数，其目的是防止成员函数修改被调用对象的值
    inline const vec3& operator+() const
    {
        return *this;
    }
    inline vec3 operator-() const
    {
        return vec3(-e[0],-e[1],-e[2]);
    }
    inline float operator [](int i) const
    {
        return e[i];
    }
    inline float& operator[](int i)
    {
        return e[i];
    }

    inline vec3& operator +=(const vec3 &v2)
    {
        e[0]+=v2.e[0];
        e[1]+=v2.e[1];
        e[2]+=v2.e[2];
        return *this;
    }  inline vec3& operator-=(const vec3 &v2)
    {
        e[0] -= v2.e[0];
        e[1] -= v2.e[1];
        e[2] -= v2.e[2];
        return *this;
    }

    inline vec3& operator*=(const vec3 &v2)
    {
        e[0] *= v2.e[0];
        e[1] *= v2.e[1];
        e[2] *= v2.e[2];
        return *this;
    }

    inline vec3& operator/=(const vec3 &v2)
    {
        e[0] /= v2.e[0];
        e[1] /= v2.e[1];
        e[2] /= v2.e[2];
        return *this;
    }

    inline vec3& operator*=(const float t)
    {
        e[0] *= t;
        e[1] *= t;
        e[2] *= t;
        return *this;
    }

    inline vec3& operator/=(const float t)
    {
        e[0] /= t;
        e[1] /= t;
        e[2] /= t;
        return *this;
    }
    inline float length() const
    {
        return std::sqrt(e[0] * e[0] + e[1] * e[1] + e[2] * e[2]);
    }
    inline float squared_length() const
    {
        return (e[0] * e[0] + e[1] * e[1] + e[2] * e[2]);
    } inline void make_unit_vector()
    {
        float len = length();
        e[0] /= len;
        e[1] /= len;
        e[2] /= len;
    }

};
inline vec3 operator+(const vec3 &v1, const vec3 &v2)
{
    return vec3(v1.e[0] + v2.e[0], v1.e[1] + v2.e[1], v1.e[2] + v2.e[2]);
}

inline vec3 operator-(const vec3 &v1, const vec3 &v2)
{
    return vec3(v1.e[0] - v2.e[0], v1.e[1] - v2.e[1], v1.e[2] - v2.e[2]);
}

inline vec3 operator*(const vec3 &v1, const vec3 &v2)
{
    return vec3(v1.e[0] * v2.e[0], v1.e[1] * v2.e[1], v1.e[2] * v2.e[2]);
}

inline vec3 operator*(const vec3 &v1, float t)
{
    return vec3(v1.e[0] * t, v1.e[1] * t, v1.e[2] * t);
}

inline vec3 operator/(const vec3 &v1, const vec3 &v2)
{
    return vec3(v1.e[0] / v2.e[0], v1.e[1] / v2.e[1], v1.e[2] / v2.e[2]);
}

inline vec3 operator/(const vec3 &v1, float t)
{
    return vec3(v1.e[0] / t, v1.e[1] / t, v1.e[2] / t);
}inline float dot(const vec3 &v1, const vec3 &v2)
{
    return v1.e[0] * v2.e[0] + v1.e[1] * v2.e[1] + v1.e[2] * v2.e[2];
}
inline vec3 cross(const vec3 &v1, const vec3 &v2)
{
    return vec3(
        (v1.e[1]*v2.e[2]-v1.e[2]*v2.e[1]),
        (-(v1.e[0]*v2.e[2] - v1.e[2]*v2.e[0])),
        (v1.e[0]*v2.e[1] - v1.e[1]*v2.e[0])
    );
}

inline vec3 unit_vector(vec3 v)
{
    return v / v.length();
}

