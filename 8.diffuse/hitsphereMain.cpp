#include "wx/wx.h"
#include <wx/dcbuffer.h>


#include "vec3.h"
#include "ray.h"
#include "hitable_list.h"
#include "float.h"
#include "sphere.h"
#include "camera.h"
#include "drand48.h"

//定义主类：继承wxApp
class MyApp : public wxApp
{
public:
    //覆盖OnInit函数
    virtual bool OnInit();
};

//定义Frame类：继承wxFrame
class MyFrame : public wxFrame
{
public:

    MyFrame(const wxString& title);//构造函数
    void OnQuit(wxCommandEvent& event);//退出时触发
    void OnMotion(wxMouseEvent& event);//鼠标拖拽触发
    void DrawRGB(wxPaintEvent& event);//重新绘画时触发
    void OnPaint(wxPaintEvent& event);//重新绘画时触发

private:
    DECLARE_EVENT_TABLE()
};

DECLARE_APP(MyApp)
IMPLEMENT_APP(MyApp)
/**
这个函数生成单位圆里的随机向量，2*(rand,rand,rand)的每个坐标的范围时(0,2),2*(rand,rand,rand)-1的每个坐标范围是(-1,1)
*/
vec3 random_in_unit_sphere()
{
    return 2.0f*vec3(float(drand48()), float(drand48()), float(drand48())) - vec3(1, 1, 1);
}

/**
碰撞函数,接受光线返回颜色
*/
vec3 genColor(const ray& r, hitable* world)
{
    //unit是屏幕上的点的方向为单位向量
    hit_record rec;
    //如果射线击中了世界里的物体
    if (world->hit(r, 0.0, FLT_MAX, rec))
    {
        //target=击中点的法线+单位球中的随机向量,如果做击中点的切面，target总是和法线在平面的同侧，符合光线随机反弹效果
         vec3 target =  rec.normal + random_in_unit_sphere();
         //递归调用，反弹射线起点是击中点，方向是target
         //递归结束的条件是光线没有击中物体
        return 0.5f*genColor(ray(rec.p, target), world);

    }
    //没有击中显示背景色
    else
    {
        vec3 unit_direction = unit_vector(r.direction());
        float t = 0.5f * (unit_direction.y() + 1.0f);
        return (1.0f - t)*vec3(1.0f, 1.0f, 1.0f) + t*vec3(0.5f, 0.7f, 1.0f);
    }

    return vec3(0.0f, 0.0f, 0.0f);

}
bool MyApp::OnInit()
{
    //关联Frame，初始化
    MyFrame* frame = new MyFrame(wxT("Minimal wxWidgets App"));
    frame->Show(true);
    frame->SetSize(600, 300);
    return true;
}


BEGIN_EVENT_TABLE(MyFrame, wxFrame)
    EVT_MENU(wxID_EXIT, MyFrame::OnQuit)
    EVT_MOTION(MyFrame::OnMotion)
    EVT_PAINT(MyFrame::OnPaint)
END_EVENT_TABLE()

MyFrame::MyFrame(const wxString& title) : wxFrame(NULL, wxID_ANY, title) {}

void MyFrame::OnQuit(wxCommandEvent& event)
{
    Close();
}

wxPoint pointbefore ;
void MyFrame::OnMotion(wxMouseEvent& event)
{
    wxClientDC dc(this);
    wxPen pen(*wxRED, 6);
    dc.SetPen(pen);
    wxPoint point;
    if (event.Dragging())
    {
        point = event.GetPosition();
        //dc.DrawPoint(event.GetPosition());
        dc.DrawLine(pointbefore,point);
        pointbefore = point;
    }
    else
    {
        pointbefore  = event.GetPosition();
    }
}
//第2章向量类测试
void MyFrame::DrawRGB(wxPaintEvent& event)
{
    int ns = 10;
//需要引入 <wx/dcbuffer.h>
    wxClientDC dc(this);
    //wxBufferedPaintDC dc(this);//用这个会让背景变黑
    //窗口像素坐标长和宽
    wxCoord w = GetClientSize().x, h = GetClientSize().y;

    //hitable类型的数组大小2
    hitable *list[2];
    //添加两个球，由球心和半径定义
    list[0] = new sphere(vec3(0.0f, 0.0f, -1.0f), 0.5f);
    list[1] = new sphere(vec3(0.0f, -100.5f, -1.0f), 100.0f);
    hitable *world = new hitable_list(list, 2);
    wxColour color(0, 0, 0, 255);
    wxPen pen(color);
    camera cam;
    for (int x = 0; x < w; x++)
    {
        for (int y = 0; y < h; y++)
        {
            vec3 col(0, 0, 0);
            for (int s = 0; s < ns; s++)
            {
                //在世界坐标中uv代表屏幕上的点
                float u = float(x+ drand48()) / float(w);
                float v = float(y+ drand48()) / float(h);
                //生成光线(原点，遍历点)
                ray r = cam.get_ray(u, v);
                //生成光线的颜色
                col += genColor(r, world);
            }
            col /= float(ns);
            int ir = int(255.99 * col[0]);
            int ig = int(255.99 * col[1]);
            int ib = int(255.99 * col[2]);

            color.Set(ir, ig, ib, 255);
            pen.SetColour(color);
            dc.SetPen(pen);
            dc.DrawPoint(x, y);
        }
    }

}
void MyFrame::OnPaint(wxPaintEvent& event)
{
    DrawRGB(event);
}


