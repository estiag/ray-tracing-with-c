#ifndef SPHEREH
#define SPHEREH

#include "hitable.h"
#include "material.h"

class sphere : public hitable
{
public:
    sphere(){}
    sphere(vec3 cen, float r, material* mat) : center(cen), radius(r), material_ptr(mat){};
    virtual bool hit(const ray& r, float tmin, float tmax, hit_record& rec) const
    {

        vec3 oc = r.origin() - center;
        float a = dot(r.direction(), r.direction());
        float b = dot(oc, r.direction());
        float c = dot(oc, oc) - radius*radius;
        float discriminant = b*b - a*c;
        if (discriminant > 0)
        {
            //�ȿ�����
            float temp = (-b - sqrt(b*b - a*c)) / a;
            if (temp < tmax && temp>tmin)
            {
                rec.t = temp;
                rec.p = r.point_at_parameter(rec.t);
                rec.normal = (rec.p - center) / radius;
                rec.mat_ptr = material_ptr;
                return true;
            }
            temp = (-b + sqrt(b*b - a*c)) / a;
            if (temp<tmax && temp>tmin)
            {
                rec.t = temp;
                rec.p = r.point_at_parameter(rec.t);
                rec.normal = (rec.p - center) / radius;
                rec.mat_ptr = material_ptr;
                return true;
            }
        }

        return false;
    }

    vec3 center;
    float radius;
    material* material_ptr;
};


#endif