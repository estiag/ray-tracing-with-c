#ifndef CAMERAH
#define CAMERAH

#include "ray.h"

class camera
{
    public:
        camera()
        {
            //wxwigets屏幕坐标从左上角为原点
            upper_left_corner = vec3(-2.0, 1.0, -1.0);
            horizontal = vec3(4.0, 0.0, 0.0);
            vertical = vec3(0.0, 2.0, 0.0);
            origin = vec3(0.0, 0.0, 0.0);
        }
        /**
        *世界坐标中u是x轴方向上屏幕坐标和世界坐标的变换比率
        *用u*horizontal就是世界坐标中对应的点的x坐标
        */
        ray get_ray(float u, float v)
        {
            return ray(origin, upper_left_corner + u*horizontal - v*vertical);
        }

        vec3 origin;
        vec3 upper_left_corner;
        vec3 horizontal;
        vec3 vertical;
};

#endif
