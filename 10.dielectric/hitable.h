#ifndef HITABLEH
#define HITABLEH

#include "ray.h"

class material;

struct hit_record
{
    float t; //相交点直线的参数t
    vec3 p;  //根据参数t求得的p
    vec3 normal; //p点的法线
    material *mat_ptr; //光线相交处的材质
};

class hitable
{
public:
    virtual bool hit(const ray& r, float t_min, float t_max, hit_record& rec) const = 0;

};


#endif