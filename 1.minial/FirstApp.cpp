#include "wx/wx.h"
#include <wx/dcbuffer.h>
class MyApp : public wxApp
{
public:
    virtual bool OnInit();
};


class MyFrame : public wxFrame
{
public:
    MyFrame(const wxString& title);
    void OnQuit(wxCommandEvent& event);

private:

    DECLARE_EVENT_TABLE()
};


DECLARE_APP(MyApp)
IMPLEMENT_APP(MyApp)

bool MyApp::OnInit()
{

    MyFrame *frame = new MyFrame(wxT("Minimal wxWidgets App"));
    frame->Show(true);
    return true;
}


BEGIN_EVENT_TABLE(MyFrame, wxFrame)
    EVT_MENU(wxID_EXIT,  MyFrame::OnQuit)
END_EVENT_TABLE()


void MyFrame::OnQuit(wxCommandEvent& event)
{

    Close();
}



MyFrame::MyFrame(const wxString& title): wxFrame(NULL, wxID_ANY, title)
{
}

